--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
--Date        : Mon Jan  2 23:04:35 2023
--Host        : wzab running 64-bit Debian GNU/Linux bookworm/sid
--Command     : generate_target dma_test_bd_wrapper.bd
--Design      : dma_test_bd_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity dma_test_bd_wrapper is
  port (
    pci_express_x8_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_perstn : in STD_LOGIC;
    pcie_refclk_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    pcie_refclk_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end dma_test_bd_wrapper;

architecture STRUCTURE of dma_test_bd_wrapper is
  component dma_test_bd is
  port (
    pcie_perstn : in STD_LOGIC;
    pcie_refclk_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    pcie_refclk_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    pci_express_x8_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pci_express_x8_txp : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component dma_test_bd;
begin
dma_test_bd_i: component dma_test_bd
     port map (
      pci_express_x8_rxn(7 downto 0) => pci_express_x8_rxn(7 downto 0),
      pci_express_x8_rxp(7 downto 0) => pci_express_x8_rxp(7 downto 0),
      pci_express_x8_txn(7 downto 0) => pci_express_x8_txn(7 downto 0),
      pci_express_x8_txp(7 downto 0) => pci_express_x8_txp(7 downto 0),
      pcie_perstn => pcie_perstn,
      pcie_refclk_clk_n(0) => pcie_refclk_clk_n(0),
      pcie_refclk_clk_p(0) => pcie_refclk_clk_p(0)
    );
end STRUCTURE;
