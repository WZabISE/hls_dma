set_property PACKAGE_PIN AD6 [get_ports {pcie_refclk_clk_p[0]}]
set_property PACKAGE_PIN E33 [get_ports pcie_perstn]
set_property IOSTANDARD LVCMOS18 [get_ports pcie_perstn]
