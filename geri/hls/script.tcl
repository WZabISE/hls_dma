open_project proj
set_top dma1
add_files src/dma1.cc
add_files src/data_gen.cc
add_files -tb src/dma1_tb.cc -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xc7vx330t-ffg1157-2}
create_clock -period 4 -name default
config_export -description {WZab DMA core written in HLS} -display_name WZ_DMA_HLS -format ip_catalog -rtl vhdl -vendor WZab -version 1.35 -vivado_optimization_level 2 -vivado_phys_opt place -vivado_report_level 0
config_sdx -target none
config_interface -clock_enable=0 -expose_global=0 -m_axi_addr64 -m_axi_offset direct -register_io off -trim_dangling_port=0
#source "./dma1/solution1/directives.tcl"
#csim_design -clean
csynth_design
set Revision [expr [clock seconds] / 10]
#cosim_design -O -wave_debug -trace_level port -argv {-debug} -tool xsim
export_design -rtl vhdl -format ip_catalog \
   -ipname "dma1" \
   -description "WZab DMA core written in HLS" -vendor "WZab"\
   -version 1.35.$Revision -display_name "WZ_DMA_HLS"
