#include <cstdlib>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define BUFLEN (1024*1024*2/8)

void dma1 (volatile AXI_VALUE *a){
#pragma HLS INTERFACE m_axi depth=50 port=a offset=slave

  int i;
  AXI_VALUE buff[50];

  //memcpy creates a burst access to memory
  //multiple calls of memcpy cannot be pipelined and will be scheduled sequentially
  //memcpy requires a local buffer to store the results of the memory transaction
  memcpy(buff,(const int*)a,50*sizeof(int));

  for(i=0; i < 50; i++){
    buff[i] = buff[i] + 100;
  }

  memcpy((AXI_VALUE *)a,buff,50*sizeof(AXI_VALUE));
}
