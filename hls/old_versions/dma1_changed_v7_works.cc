#include <cstdlib>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define BUFLEN (1024*1024*2/8)
const int TMPBUFLEN=256;

void dma1 (AXIS_DATA &din, volatile AXI_VALUE *a){
#pragma HLS INTERFACE axis port=din
#pragma HLS INTERFACE m_axi depth=TMPBUFLEN port=a offset=slave

  int i;
  AXI_VALUE buff[TMPBUFLEN];

  //memcpy creates a burst access to memory
  //multiple calls of memcpy cannot be pipelined and will be scheduled sequentially
  //memcpy requires a local buffer to store the results of the memory transaction

  for(i=0; i < TMPBUFLEN; i++){
    buff[i] = din.data;
    if(din.last) break;
  }

  memcpy((AXI_VALUE *)a,buff,i*sizeof(AXI_VALUE));
}
