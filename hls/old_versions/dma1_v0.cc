#include <cstdlib>
#include <ap_int.h>
#include <ap_axi_sdata.h>
typedef ap_uint<64> AXI_VALUE;
typedef ap_axiu<64,0,0,0> AXIS_DATA;

#define BUFLEN (1024*1024*2/8)

void dma1(AXIS_DATA & din, ap_uint<1> ena, AXI_VALUE * mout)
{
#pragma HLS_INTERFACE m_axi port=mout offset=slave bundle=gmem0
#pragma HLS_INTERFACE axis port = din
#pragma HLS_INTERFACE ap_none port = ena
	int i=0;
	if(ena) {
		AXI_VALUE data = din.data;
		*mout = data;
		i++;
		if(i >= BUFLEN)
			i = 0;
		if(din.last) {
			//End of the packet, fill the descriptor info
			{};
		}
	}

}
