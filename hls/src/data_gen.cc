// Created by Wojciech M. Zabolotny (wzab01<at>gmail.com or wojciech.zabolotny<at>pw.edu.pl)
// License: Dual GPL/BSD

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>
#include "dma1_defs.h"

static int base = 0x1000;
//Function gen_data generates random values in the stream.
void gen_data(hls::stream<AXIS_DATA> &stin) {
#pragma HLS DATAFLOW
	int pkt_len = 10 + (random() & 0xff);
	for (int i = 0; i < pkt_len; i++) {
#pragma HLS PIPELINE
		AXIS_DATA x;
		x.data = base++;
		x.last = 0;
		if (i == pkt_len - 1) {
			x.last = 1;
			x.data = 0xa5a5;
			base += 0xfff;
		}
		stin.write(x);
	}
}
