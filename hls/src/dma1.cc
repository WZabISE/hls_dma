#include <cstdlib>
#include <stdint.h>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>
#include "dma1_defs.h"

/* Implementation of the DMA core in HLS
 * Copyright (C) 2021-2023 by Wojciech M. Zabolotny
 * wzab<at>ise.pw.edu.pl, wojciech.zabolotny<at>pw.edu.pl
 *
 * Development of this code is partially supported by the
 * European Union’s Horizon 2020 research and innovation programme
 * under grant agreement No 871072.
 *
 * The code is based on https://github.com/Xilinx/HLS-Tiny-Tutorials/blob/master/interface_axi_stream_to_master/example.cpp
 * New link: https://github.com/Xilinx/Vitis-HLS-Introductory-Examples/blob/master/Interface/Streaming/axi_stream_to_master/example.cpp
 *
 * My contribution is licensed under the license: Dual GPL/BSD
 */

static void readin(hls::stream<AXIS_DATA> &stin,
		hls::stream<BUF_DATA> &buf_stream,
		hls::stream<BURST_MARK> &burst_marks) {
	AXIS_DATA in_val;

	static int count = 1; // Number of words in current burst
#pragma HLS RESET variable = count

	static int nr_word = 1; // Number of words in the current buffer
#pragma HLS RESET variable = nr_word

	static int bufleft = BUFLEN - 1; // Number of words remained in the current buffer
#pragma HLS RESET variable = bufleft

	do {
#pragma HLS PIPELINE
		stin.read(in_val);
		BUF_DATA out_val = { in_val.data };
		buf_stream.write(out_val);
		if (count >= MAX_BURST_LENGTH || in_val.last || (bufleft == 0)) {
			BURST_MARK bm = { 0, 0, 0, 0};
			bm.count = count;
			bm.word = nr_word;
			bm.eop = in_val.last;
			bm.nextbuf = 0;
			if (bufleft == 0) {
				bm.nextbuf = 1;
				nr_word = 1;
				bufleft = BUFLEN - 1;
			} else {
				nr_word += 1;
				bufleft -= 1;
			}
			count = 1;
			burst_marks.write(bm);
		} else {
			count += 1;
			nr_word += 1;
			bufleft -= 1;
		}
	} while (!in_val.last);
}

static void prepare(hls::stream<BURST_MARK> &burst_marks,
		hls::stream<OUTPUT_CHUNK> &chunks, volatile uint32_t &i_cur_pkt,
		ap_uint<32> &nof_bufs, volatile uint32_t &i_cur_buf,
		AXI_ADDR bufs[NBUFS]) {

	static ap_uint<1> soverrun = 0;
#pragma HLS RESET variable = soverrun

	static ap_uint<32> i_prev_buf = 0;
#pragma HLS RESET variable = i_prev_buf

	static int i_prev_word = 0;
#pragma HLS RESET variable = i_prev_word

	static ap_uint<32> i_nr_buf = 0;
#pragma HLS RESET variable = i_nr_buf
	static ap_uint<32> i_nr_pkt = 0;
#pragma HLS RESET variable = i_nr_pkt
	BURST_MARK bm;
	do {
#pragma HLS PIPELINE II=3	
		OUTPUT_CHUNK chunk = { 0, 0, 0, 0, 0, 0, 0, 0};
		burst_marks.read(bm);
		ap_uint<32> new_pkt;
		ap_uint<32> new_buf;
		ap_uint<1> overrun_pkt = 0;
		ap_uint<1> overrun_buf = 0;
		//Calculate the parameters of the chunk based on current state of packets
		if (soverrun == 0) {
			chunk.packet = i_nr_pkt;
			chunk.base = bufs[i_nr_buf] / 32 + bm.word - bm.count;
			chunk.count = bm.count;
			chunk.eop = bm.eop;
			//Remember that BUFLEN is already divided by 32!
			chunk.first = i_prev_buf * BUFLEN + i_prev_word;
			chunk.after = i_nr_buf * BUFLEN + bm.word;
			//Now we check for the overrun condition
			//Is there a place for the next buffer?
			if (bm.eop) {
				i_prev_buf = i_nr_buf;
				i_prev_word = bm.word;
				new_pkt = (i_nr_pkt + 1) % NPKTS;
				if (new_pkt == i_cur_pkt) {
					overrun_pkt = 1;
				} else {
					i_nr_pkt = new_pkt;
				}
			}
			//Is there a place for the next packet?
			if (bm.nextbuf) {
				new_buf = i_nr_buf + 1;
				if ((new_buf == NBUFS) || (new_buf == nof_bufs)) {
					new_buf = 0;
				}
				if (new_buf == i_cur_buf) {
					overrun_buf = 1;
				} else {
					i_nr_buf = new_buf;
				}
			}
			if ((overrun_buf == 1) || (overrun_pkt == 1))
				soverrun = 1;
			chunk.overrun = soverrun;
			chunk.nr_buf = i_nr_buf;
			chunks.write(chunk);
		}
	} while (!bm.eop);
}

void writeout(hls::stream<BUF_DATA> &buf_stream,
		hls::stream<OUTPUT_CHUNK> &chunks, hls::stream<OUTPUT_SIGS> &outs,
		AXI_VALUE *a, AXI_ADDR bufs[NBUFS], AXI_ADDR &descs) {

	BUF_DATA vin;
	OUTPUT_CHUNK chunk;
	AXI_VALUE *b;
	int i;
	static OUTPUT_SIGS os = { 0, 0, 0, 0};
#pragma HLS RESET variable = os
	do {
#pragma HLS PIPELINE
		chunks.read(chunk);
		if (!chunk.overrun) {
			int i;
			int j = chunk.base;
			b = &a[j];
			for (i = 0; i < chunk.count; i++) {
#pragma HLS PIPELINE
				vin = buf_stream.read();
				b[i] = vin.dta;
			}
			os.nr_buf = chunk.nr_buf;
		}
		if (chunk.overrun == 1)
			os.overrun = 1;
		if (chunk.eop) {
			//Write start and end positions to the packet descriptor
			PKT_DESC sd = { 0, 0, 0, 0 };
			sd.first = htole64(chunk.first);
			sd.after = htole64(chunk.after);
			a[descs / 32 + chunk.packet] = *(ap_uint<256> *) &sd;
			os.nr_pkt = chunk.packet;
		}
		os.eop = chunk.eop;
		outs.write(os);
	} while (!chunk.eop);
}

void update_outs(hls::stream<OUTPUT_SIGS> &outs, ap_uint<32> &nr_pkt,
		ap_uint<32> &nr_buf, ap_uint<1> &overrun) {
	OUTPUT_SIGS os;
#pragma HLS PIPELINE
	do {
		outs.read(os);
		nr_pkt = os.nr_pkt;
		nr_buf = os.nr_buf;
		overrun = os.overrun;
	} while (!os.eop);
}

void dma1(hls::stream<AXIS_DATA>&stin, AXI_VALUE *a, AXI_ADDR bufs[NBUFS],
		AXI_ADDR descs, ap_uint<32> nof_bufs, volatile uint32_t &cur_buf,
		ap_uint<32> &nr_buf, volatile uint32_t &cur_pkt, ap_uint<32> &nr_pkt,
		ap_uint<1> &xoverrun) {
#pragma HLS INTERFACE axis port=stin
#pragma HLS INTERFACE m_axi max_write_burst_length=256 \
		 num_read_outstanding=8 \
		 num_write_outstanding=8 \
		 max_read_burst_length=16 \
	  offset=direct depth=1<<64 bundle=gmem0 port=a

//    latency=200

#pragma HLS INTERFACE ap_ctrl_hs port=return

#pragma HLS INTERFACE s_axilite port=bufs depth=NBUFS bundle=control
#pragma HLS INTERFACE s_axilite port=descs bundle=control
#pragma HLS INTERFACE s_axilite port=nof_bufs bundle=control

#pragma HLS INTERFACE ap_none port=cur_buf
#pragma HLS INTERFACE ap_none port=nr_buf
#pragma HLS INTERFACE ap_none port=cur_pkt
#pragma HLS INTERFACE ap_none port=nr_pkt
#pragma HLS INTERFACE ap_none port=xoverrun

#pragma HLS DATAFLOW

	hls::stream<BUF_DATA, DATA_DEPTH> buf;
	hls::stream<BURST_MARK, COUNT_DEPTH> bursts;
	hls::stream<OUTPUT_CHUNK, CHUNKS_DEPTH> chunks;
	hls::stream<OUTPUT_SIGS, 2> outs;
	readin(stin, buf, bursts);
	prepare(bursts, chunks, cur_pkt, nof_bufs, cur_buf, bufs);
	writeout(buf, chunks, outs, a, bufs, descs);
	update_outs(outs, nr_pkt, nr_buf, xoverrun);
}
