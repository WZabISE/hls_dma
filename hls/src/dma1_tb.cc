// Created by Wojciech M. Zabolotny (wzab01<at>gmail.com or wojciech.zabolotny<at>pw.edu.pl)
// License: Dual GPL/BSD

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>
#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>
#include "dma1_defs.h"

#define HOST_RAM_SIZE (1<<20L)
void gen_data(hls::stream<AXIS_DATA> &stin);

void dma1(hls::stream<AXIS_DATA>&stin, AXI_VALUE *a,
		AXI_ADDR bufs[NBUFS], AXI_ADDR descs, ap_uint<32> nof_bufs, volatile uint32_t &cur_buf,
		ap_uint<32> &nr_buf, volatile uint32_t &cur_pkt, ap_uint<32> &nr_pkt,
		ap_uint<1> &xoverrun);

//Table emulating the host RAM (limited size to enable simulation!)
static AXI_VALUE host_ram[HOST_RAM_SIZE] = { 0 };

AXI_ADDR bufs[NBUFS] = { 0x10000, 0x12000, 0x11000, 0x13000};
ap_uint<32> nof_bufs = 4;
hls::stream<AXIS_DATA,3000> stin;
AXI_ADDR descs = 0x100;
uint32_t cur_buf = 0;
ap_uint<32> nr_buf = 0;
uint32_t cur_pkt = 0;
ap_uint<32> nr_pkt = 0;
ap_uint<1> xoverrun = 0;
ap_uint<1> pkt_av = 0;


int main()
{
	//std::ofstream dtafile;
	//dtafile.open("indata.txt");
	//Initialize emulated RAM
	for(int i = 0 ; i < HOST_RAM_SIZE ; i++) host_ram[i] = 0;
	// A few cycles before the run is initialized
	std::cout << "Initialization finished";
    for(int i=0;i<3;i++) {
       gen_data(stin);
       dma1(stin, host_ram, bufs, descs, nof_bufs, cur_buf, nr_buf , cur_pkt, nr_pkt, xoverrun);
    }
    //AXIS_DATA x;
    //x.user = 1;
    //stin.write(x);
    std::ofstream resfile;
    resfile.open ("results.txt");
    resfile << "START" << std::endl;
    for(int i = 0 ; i < HOST_RAM_SIZE; i++) {
    	if(host_ram[i] != 0) {
    		resfile << std::hex << i << " : " << host_ram[i] << std::endl;
    	}
    }
    resfile << "END" << std::endl;
    resfile.close();
    std::cout << "Finished" << std::endl;

}
