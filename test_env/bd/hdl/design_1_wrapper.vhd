--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
--Date        : Tue Aug  9 16:09:57 2022
--Host        : wzab running 64-bit Debian GNU/Linux bookworm/sid
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    clk : in STD_LOGIC;
    din_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    din_tdest : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tid : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    din_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tready : out STD_LOGIC;
    din_tstrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    din_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tvalid : in STD_LOGIC;
    overrun : out STD_LOGIC_VECTOR ( 0 to 0 );
    rst_n : in STD_LOGIC;
    run : out STD_LOGIC;
    pkt_av : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    rst_n : in STD_LOGIC;
    run : out STD_LOGIC;
    pkt_av : out STD_LOGIC_VECTOR ( 0 to 0 );
    overrun : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC;
    din_tvalid : in STD_LOGIC;
    din_tready : out STD_LOGIC;
    din_tdata : in STD_LOGIC_VECTOR ( 255 downto 0 );
    din_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
    din_tstrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    din_tuser : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tlast : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tid : in STD_LOGIC_VECTOR ( 0 to 0 );
    din_tdest : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      clk => clk,
      din_tdata(255 downto 0) => din_tdata(255 downto 0),
      din_tdest(0) => din_tdest(0),
      din_tid(0) => din_tid(0),
      din_tkeep(31 downto 0) => din_tkeep(31 downto 0),
      din_tlast(0) => din_tlast(0),
      din_tready => din_tready,
      din_tstrb(31 downto 0) => din_tstrb(31 downto 0),
      din_tuser(0) => din_tuser(0),
      din_tvalid => din_tvalid,
      overrun(0) => overrun(0),
      rst_n => rst_n,
      run => run,
      pkt_av(0) => pkt_av(0)
    );
end STRUCTURE;
