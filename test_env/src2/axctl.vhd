-------------------------------------------------------------------------------
-- Title      : Axi lite controller
-- Project    : 
-------------------------------------------------------------------------------
-- File       : axctl.vhd
-- Author     : Wojciech M. Zabo??otny  <wzab01@gmail.com>
-- Company    : 
-- Created    : 2021-07-28
-- Last update: 2022-01-16
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2021 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-07-28  1.0      WZab      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity axctl is
  generic (
    S_AXI_AW : integer := 15;
    S_AXI_DW : integer := 32);
  port (
    -- AXI Lite interface
    s_axi_AWVALID : out std_logic;
    s_axi_AWREADY : in  std_logic;
    s_axi_AWADDR  : out std_logic_vector (S_AXI_AW-1 downto 0);
    s_axi_WVALID  : out std_logic;
    s_axi_WREADY  : in  std_logic;
    s_axi_WDATA   : out std_logic_vector (S_AXI_DW-1 downto 0);
    s_axi_WSTRB   : out std_logic_vector (S_AXI_DW/8-1 downto 0);
    s_axi_ARVALID : out std_logic;
    s_axi_ARREADY : in  std_logic;
    s_axi_ARADDR  : out std_logic_vector (S_AXI_AW-1 downto 0);
    s_axi_RVALID  : in  std_logic;
    s_axi_RREADY  : out std_logic;
    s_axi_RDATA   : in  std_logic_vector (S_AXI_DW-1 downto 0);
    s_axi_RRESP   : in  std_logic_vector (1 downto 0);
    s_axi_BVALID  : in  std_logic;
    s_axi_BREADY  : out std_logic;
    s_axi_BRESP   : in  std_logic_vector (1 downto 0);
    -- User signals
    run           : out std_logic := '0';
    -- HLS block control signals
    ap_start : out std_logic := '0';
    ap_done : in std_logic;
    ap_ready : in std_logic;
    ap_idle : in std_logic;
    -- System interface
    clk           : in  std_logic;
    arst_n        : in  std_logic
    );

end entity axctl;

architecture beh of axctl is

  signal arst_p : std_logic;

  signal wb_cyc : std_logic := '0';
  signal wb_stb : std_logic := '0';
  signal wb_we  : std_logic := '0';

  signal wb_cti : std_logic_vector(2 downto 0);
  signal wb_bte : std_logic_vector(1 downto 0);

  signal wb_stall : std_logic := '0';
  signal wb_ack   : std_logic := '0';
  signal wb_err   : std_logic := '0';

  signal wb_wdata : std_logic_vector(31 downto 0) := (others => '0');
  signal wb_rdata : std_logic_vector(31 downto 0) := (others => '0');
  signal wb_addr  : std_logic_vector(31 downto 0) := (others => '0');
  signal wb_sel   : std_logic_vector(3 downto 0)  := (others => '0');

  signal pwb_cyc : std_logic := '0';
  signal pwb_stb : std_logic := '0';
  signal pwb_we  : std_logic := '0';

  signal pwb_cti : std_logic_vector(2 downto 0);
  signal pwb_bte : std_logic_vector(1 downto 0);

  signal pwb_stall : std_logic := '0';
  signal pwb_ack   : std_logic := '0';
  signal pwb_err   : std_logic := '0';

  signal pwb_wdata : std_logic_vector(31 downto 0) := (others => '0');
  signal pwb_rdata : std_logic_vector(31 downto 0) := (others => '0');
  signal pwb_addr  : std_logic_vector(31 downto 0) := (others => '0');
  signal pwb_sel   : std_logic_vector(3 downto 0)  := (others => '0');


begin  -- architecture beh

  arst_p <= not arst_n;

  process is

    procedure wb_write (
      constant addr : in unsigned(31 downto 0);
      constant val  : in unsigned(31 downto 0)) is
    begin  -- procedure wb_write
      wait until rising_edge(clk);
      wb_we    <= '1';
      wb_sel   <= (others => '1');
      wb_addr  <= std_logic_vector(addr/4);
      wb_wdata <= std_logic_vector(val);
      wait until rising_edge(clk);
      wb_cyc   <= '1';
      wb_stb   <= '1';
      wl1 : loop
        wait until rising_edge(clk);
        if wb_err = '1' then
          report "write error" severity failure;
        end if;
        if wb_ack = '1' then
          exit wl1;
        end if;
      end loop wl1;
      wb_we  <= '0';
      wb_cyc <= '0';
      wb_sel <= (others => '0');
      wb_stb <= '0';
      wait until rising_edge(clk);
    end procedure wb_write;

    procedure wb_iwrite (
      constant addr : in integer;
      constant val  : in integer) is
      variable v_addr : unsigned(31 downto 0);
      variable v_val : unsigned(31 downto 0);      
    begin  -- procedure wb_iwrite
      v_addr := to_unsigned(addr,32);
      v_val := to_unsigned(val,32);
      wb_write(v_addr, v_val);
    end procedure wb_iwrite;

    procedure wb_read (
      constant addr : in unsigned(31 downto 0)) is
    begin  -- procedure wb_write
      null;
    end procedure wb_read;

  begin  -- process
    wait until arst_n = '1';
    wait until rising_edge(clk);
    -- Set addresses of the buffers
    wb_write(x"00001000", x"11000000");
    wb_write(x"00001004", x"00000000");
    wb_write(x"00001008", x"12000000");
    wb_write(x"0000100c", x"00000000");
    wb_write(x"00001010", x"13000000");
    wb_write(x"00001014", x"00000000");
    wb_write(x"00001018", x"14000000");
    wb_write(x"0000101c", x"00000000");
    wb_write(x"00001020", x"15000000");
    wb_write(x"00001024", x"00000000");
    wb_write(x"00001028", x"16000000");
    wb_write(x"0000102c", x"00000000");
    wb_write(x"00001030", x"17000000");
    wb_write(x"00001034", x"00000000");
    wb_write(x"00001038", x"18000000");
    wb_write(x"0000103c", x"00000000");
    -- Program the engine
    -- Write the address of the descriptors
    wb_write(x"00002000", x"19000000");
    wb_write(x"00002004", x"00000000");
    -- Write the number of the buffers
    wb_write(x"00002000", x"19000000");
    
    -- Number of buffers
    wb_write(x"0000200c", x"00000008");
    -- Set number of the segment to check for overflow
    wb_write(x"00004000", x"00000016");
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    ap_start <= '1';
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    run <= '1';    
    wait;
  end process;

  wb_cti <= (others => '0');
  wb_bte <= (others => '0');

  wpc2p1 : entity work.wbc2pipeline
    generic map (
      AW => S_AXI_AW,
      DW => 32)
    port map (
      i_clk   => clk,
      i_reset => arst_p,
      i_scyc  => wb_cyc,
      i_sstb  => wb_stb,
      i_swe   => wb_we,
      i_saddr => wb_addr(S_AXI_AW-1 downto 0),
      i_sdata => wb_wdata,
      i_ssel  => wb_sel,
      i_scti  => wb_cti,
      i_sbte  => wb_bte,
      o_sack  => wb_ack,
      o_sdata => wb_rdata,
      o_serr  => wb_err,

      o_mcyc   => pwb_cyc,
      o_mstb   => pwb_stb,
      o_mwe    => pwb_we,
      o_maddr  => pwb_addr(S_AXI_AW-1 downto 0),
      o_mdata  => pwb_wdata,
      o_msel   => pwb_sel,
      i_mstall => pwb_stall,
      i_mack   => pwb_ack,
      i_mdata  => pwb_rdata,
      i_merr   => pwb_err
      );

  wb2axil1 : entity work.wbm2axilite
    generic map (
      C_AXI_ADDR_WIDTH => S_AXI_AW,
      C_AXI_DATA_WIDTH => 32,
      DW               => 32,
      AW               => S_AXI_AW
      )
    port map (
      i_clk         => clk,
      i_reset       => arst_p,
      -- WB part
      i_wb_cyc      => pwb_cyc,
      i_wb_stb      => pwb_stb,
      i_wb_we       => pwb_we,
      i_wb_addr     => pwb_addr(S_AXI_AW-1 downto 0),
      i_wb_data     => pwb_wdata,
      i_wb_sel      => pwb_sel,
      o_wb_stall    => pwb_stall,
      o_wb_ack      => pwb_ack,
      o_wb_data     => pwb_rdata,
      o_wb_err      => pwb_err,
      -- AXI Lite part
      o_axi_awvalid => s_axi_AWVALID,
      i_axi_awready => s_axi_AWREADY,
      o_axi_awaddr  => s_axi_AWADDR,
      o_axi_awprot  => open,            --s_axi_AWPROT,
      o_axi_wvalid  => s_axi_WVALID,
      i_axi_wready  => s_axi_WREADY,
      o_axi_wdata   => s_axi_WDATA,
      o_axi_wstrb   => s_axi_WSTRB,
      i_axi_bvalid  => s_axi_BVALID,
      o_axi_bready  => s_axi_BREADY,
      i_axi_bresp   => s_axi_BRESP,
      o_axi_arvalid => s_axi_ARVALID,
      i_axi_arready => s_axi_ARREADY,
      o_axi_araddr  => s_axi_ARADDR,
      o_axi_arprot  => open,            --s_axi_ARPROT,
      i_axi_rvalid  => s_axi_RVALID,
      o_axi_rready  => s_axi_RREADY,
      i_axi_rdata   => s_axi_RDATA,
      i_axi_rresp   => s_axi_RRESP
      );


end architecture beh;
