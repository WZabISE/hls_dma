-------------------------------------------------------------------------------
-- Title      : Axi slave monitor
-- Project    : 
-------------------------------------------------------------------------------
-- File       : axmon.vhd
-- Author     : FPGA Developer  <xl@wzab.nasz.dom>
-- Company    : 
-- Created    : 2021-06-20
-- Last update: 2021-12-13
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2021 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-06-20  1.0      xl      Created
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity axmon is

  port (
    clk     : in  std_logic;
    arst_n  : in  std_logic;
    o_rdata : out std_logic_vector(255 downto 0);
    i_wdata : in  std_logic_vector(255 downto 0);
    i_raddr : in  std_logic_vector(58 downto 0);
    i_waddr : in  std_logic_vector(58 downto 0);
    i_wstrb : in  std_logic_vector(31 downto 0);
    i_we    : in  std_logic;
    i_rd    : in  std_logic
    );

end entity axmon;

architecture beh of axmon is

  -- based on http://edaplaygroundblog.blogspot.com/2018/10/how-to-convert-stdlogicvector-to-hex.html
  function to_hstring (SLV : std_logic_vector) return string is
    variable L : LINE;
  begin
    hwrite(L,SLV);
    return L.all;
  end function to_hstring;
  
begin  -- architecture beh

  process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if arst_n = '0' then              -- synchronous reset (active low)
        null;
      else
        if i_rd = '1' then
          o_rdata <= i_raddr & std_logic_vector(to_unsigned(0,256-59));
        end if;
        if i_we = '1' then
          report "Writing " & to_hstring(i_wdata) & " to " & to_hstring(i_waddr);
        end if;
      end if;
    end if;
  end process;

end architecture beh;
