-------------------------------------------------------------------------------
-- Title      : Testbench for design "design_1_wrapper"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : design_1_wrapper_tb.vhd
-- Author     : FPGA Developer  <xl@wzab.nasz.dom>
-- Company    : 
-- Created    : 2021-06-20
-- Last update: 2021-12-13
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2021 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2021-06-20  1.0      xl      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity design_1_wrapper_tb is

end entity design_1_wrapper_tb;

-------------------------------------------------------------------------------

architecture sim of design_1_wrapper_tb is

  -- component ports
  signal din_tdata  : std_logic_vector (255 downto 0);
  signal din_tdest  : std_logic_vector (0 downto 0) := (others => '0');
  signal din_tid    : std_logic_vector (0 downto 0) := (others => '0');
  signal din_tkeep  : std_logic_vector (31 downto 0);
  signal din_tlast  : std_logic_vector (0 to 0);
  signal din_tready : std_logic;
  signal din_tstrb  : std_logic_vector (31 downto 0);
  signal din_tuser  : std_logic_vector (0 downto 0) := (others => '0');
  signal din_tvalid : std_logic;
  signal rst_n      : std_logic := '0';
  signal run        : std_logic := '0';
  signal overrun    : std_logic_vector (0 to 0);
  signal pkt_av     : std_logic_vector (0 to 0);

  -- clock
  signal clk : std_logic := '1';

begin  -- architecture sim

  -- component instantiation
  DUT : entity work.design_1_wrapper
    port map (
      clk        => clk,
      run        => run,
      overrun    => overrun,
      pkt_av     => pkt_av,
      din_tdata  => din_tdata,
      din_tdest  => din_tdest,
      din_tid    => din_tid,
      din_tkeep  => din_tkeep,
      din_tlast  => din_tlast,
      din_tready => din_tready,
      din_tstrb  => din_tstrb,
      din_tuser  => din_tuser,
      din_tvalid => din_tvalid,
      rst_n      => rst_n);

  axi4s_src3_1 : entity work.axi4s_src3
    port map (
      tdata  => din_tdata,
      tkeep  => din_tkeep,
      tstrb  => din_tstrb,
      tlast  => din_tlast(0),
      tready => din_tready,
      tvalid => din_tvalid,
      clk    => clk,
      resetn => rst_n,
      start  => run);

  -- clock generation
  Clk <= not Clk after 10 ns;

  rst_n <= '1' after 53 ns;
  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here

    wait until Clk = '1';
  end process WaveGen_Proc;



end architecture sim;

-------------------------------------------------------------------------------

configuration design_1_wrapper_tb_sim_cfg of design_1_wrapper_tb is
  for sim
  end for;
end design_1_wrapper_tb_sim_cfg;

-------------------------------------------------------------------------------
